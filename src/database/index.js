require('dotenv').config()
const pgPromise = require('pg-promise');

const pgp = pgPromise({}); // Empty object means no additional config required
let config;

if (process.env.DATABASE_URL) { // Production
    config = process.env.DATABASE_URL;
    console.log('production');
} else { // Local / Development
    config = {
        host: process.env.POSTGRES_HOST,
        port: process.env.POSTGRES_PORT,
        database: process.env.POSTGRES_DB,
        user: process.env.POSTGRES_USER,
        password: process.env.POSTGRES_PASSWORD
    };
    console.log('local');
}


const db = pgp(config);

module.exports = db;