const {
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLString,
    GraphQLList,
    GraphQLInt,
    GraphQLBoolean,
    GraphQLNonNull,
} = require('graphql');

const db = require('../database');

const CustomerType = new GraphQLObjectType({
    name: 'Customer',
    type: 'Query',
    fields: {
        id: { type: GraphQLInt },
        name: { type: GraphQLString },
        gender: { type: GraphQLString },
        phone_number: { type: GraphQLString },
        line_id: { type: GraphQLString },
        address: { type: GraphQLString },
    }
})

module.exports = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            customer: {
                type: CustomerType,
                args: {
                    id: { type: GraphQLInt}
                },
                resolve(parent, { id }) {
                    const query = 'SELECT * FROM customer WHERE id = $1';
                    const values = [id];

                    return db
                        .oneOrNone(query, values)
                        .then(res => res)
                        .catch(err => {
                            return err;
                        });
                }   
            },
            customers: {
                type: GraphQLList(CustomerType),
                resolve() {
                    const query = 'SELECT * FROM customer';
                        return db
                            .manyOrNone(query)
                            .then(res => res)
                            .catch(err => {
                                console.log(err);
                            });
                }
            }
        }
    }),
    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: {
            customer: {
                type: CustomerType,
                args: {
                    name: { type: GraphQLNonNull(GraphQLString) },
                    is_male: { type: GraphQLNonNull(GraphQLBoolean) },
                    phone_number: { type: GraphQLNonNull(GraphQLString) },
                    line_id: { type: GraphQLString },
                    address: { type: GraphQLString },
                },
                resolve(parent, args) {
                    const query = 
                        `INSERT INTO customer(
                            name,
                            gender,
                            phone_number,
                            line_id,
                            address
                        ) VALUES (
                            $1,
                            $2,
                            $3,
                            $4,
                            $5
                        )`;
                    const values = [
                        args.name,
                        args.is_male? 'Male': 'Female',
                        args.phone_number,
                        args.line_id,
                        args.address
                    ];

                    db.one(query, values)
                        .then(res => res)
                        .catch(err => err);
                },
            },
        },
    }),
});