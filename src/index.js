require('dotenv').config()

const { Server } = require('http');
const SubscriptionServer = require('subscriptions-transport-ws');
const { execute, subscribe } = require('graphql');
const GraphHTTP = require('express-graphql');
const express = require('express');

// Getting base GraphQL Schema
const schema = require('./schema');

/** BASE Express server definition **/
const app = express();

// main endpoint for GraphQL Express
app.use('/api', GraphHTTP({
    schema: schema,
    graphiql: true,
}));

// // Making plain HTTP server for Websocket usage
// const server = Server(app);

// /** GraphQL Websocket definition **/
// SubscriptionServer.create({
//     schema,
//     execute,
//     subscribe,
// }, {
//     server: server,
//     path: '/api/ws',
// });


app.listen(process.env.PORT || 4000, () => {
    console.log('Server started here -> http://0.0.0.0:4000');
});